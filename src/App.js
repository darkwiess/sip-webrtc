import React, { useEffect, useState } from 'react';
import { useForm } from "react-hook-form";
import JsSIP from 'jssip';
import './App.css';
let session, ua, options, views = {};
let streamids = [];

const GenerateRemoteElement = (props) => {

    const View = () => {
        const [videoElement, setVideoElement] = useState(null);
        useEffect(() => {
            if(videoElement === null){ setVideoElement(document.getElementById(props.stream.id));}
            if(videoElement !== null){ videoElement.srcObject = props.stream; }
        },[videoElement])

        let videoHTML = React.createElement("video", { className : "remote-video", id : props.stream.id, autoPlay : true, playsInline : true });
        
        return (
            <div className="col-sm-6 card-video">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title"> Remote</h5>
                        { videoHTML }
                    </div>
                </div>
            </div>
        )
    }
    return (<View/>)
}

const App = () => {

    const [streams, setStreams] = useState([]);
    const [sipLog, setSipLog] = useState("");
    // const [peerconnection, setPeerconnection] = useState(null);
    const [cameraStatus, setCameraStatus] = useState(false);
    const [sipDetail, setSipDetail] = useState({});
    const [controlEnable, setControlEnabled] = useState({registerButton : false, startCallButton : true, disconnectButton : true, toggleCameraButton: true})
    const { register, handleSubmit } = useForm();
    const onSubmit = data => handleRegister(data);

    const RegisterSip = (sipuri, password, websocket, iceServersUrl, iceServersUsername, iceServersPassword) => {
        var rtc = "";
        views = {
            'localVideo':  document.getElementById('local-video'),
            'remoteVideo': document.getElementById('remote-video')
        };
        var socket = new JsSIP.WebSocketInterface(websocket);
        var configuration = {
            sockets  : [ socket ],
            password : password,
            uri      : sipuri
        };
        ua = new JsSIP.UA(configuration);
        ua.start();
        ua.on("registered", function(){
            setControlEnabled({registerButton : true, startCallButton : false, toggleCameraButton: true, disconnectButton : true})
        });
        ua.on('newRTCSession', (data) => {
            
            rtc = data.session;
            // setPeerconnection(rtc)

            rtc.connection.ontrack = function (event) {
                if (event.track.kind === 'video') {
                    // event.track.enabled = false;
                }
                for (let i = 0; i < event.streams.length; ++i) {
                    event.streams[i].local = false;
                    if(streamids.indexOf(event.streams[i].id) === -1){
                        
                        streamids.push(event.streams[i].id);
                        setStreams(streams => [...streams, event.streams[i]])
                    }
                }
            };
        }); 

        var eventHandlers = {
            'progress': function(e) {
                console.log('call is in progress');
            },
            'failed': function(e) {
            },
            'ended': function(e) {
            },
            'confirmed': function(e) {
                console.log('call confirmed');
                setSipLog('call confirmed')
                views.localVideo.srcObject  = session.connection.getLocalStreams()[0];
                // views.remoteVideo.srcObject = session.connection.getRemoteStreams()[0];
                setControlEnabled({registerButton : true, startCallButton : true, toggleCameraButton: false, disconnectButton : false})
            },
            'icecandidate' : (e) => {},
            'reinvite' : (e) =>{},
            'sdp' : (e) => {
                if(e.originator === "local"){
        
                }
            }
        };
        var pcConfig = {
            "iceServers" : [{ url:"turn:turn.ttrs.in.th?transport=tcp", username: "turn01", credential:"Test1234"}],
            "bundlePolicy" : "max-compat",
            "iceTransportPolicy":"all",
            'rtcpMuxPolicy' : "negotiate",
        }
        options = {
            'eventHandlers'    : eventHandlers,
            'mediaConstraints' : {audio:true, video:true},
            'pcConfig' : pcConfig,
            'sessionTimersExpires' : 3600
        };

        setCameraStatus(true);
    }
    const handleCall = (data) => {
        try {
            session = ua.call(`sip:${sipDetail.destination}`, options);
        } catch (error) {
            console.log(error)
        }
    }
    // const handleAnswer = () => {
    //     try {
            
    //     } catch (error) {
            
    //     }
    // }

    const handleRegister = (data) => {
        try {
            if(data !== undefined){
                setSipDetail(data);
                localStorage.setItem('sipuri', data.sipuri);
                localStorage.setItem('password', data.password);
                localStorage.setItem('websocket', data.websocket);
                localStorage.setItem('destination', data.destination);
                localStorage.setItem('iceServersUrl', data.iceServersUrl)
                localStorage.setItem('iceServersUsername', data.iceServersUsername)
                localStorage.setItem('iceServersPassword', data.iceServersPassword)
                RegisterSip(data.sipuri, data.password, data.websocket, data.iceServersUrl, data.iceServersUsername, data.iceServersPassword);
            } 
        } catch (error) {
            console.log(error)
        }
    }
    const handleToggleCamera = () => {
        switch (cameraStatus) {
            case true:
                views.localVideo.srcObject.getTracks().forEach(function(track){  if(track.kind === "video"){ track.enabled = false;  }}); 
                setCameraStatus(false);
                break;
            case false:
                views.localVideo.srcObject.getTracks().forEach(function(track){  if(track.kind === "video"){ track.enabled = true;  }}); 
                setCameraStatus(true);
                break;
            default:
                break;
        }   
    }
    const handleDisconnect = () => {
        session.terminate();
        ua.stop();
        ua.unregister();
    }

    return (
        <div className="container">
            <br/>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group row">
                    <label htmlFor="staticEmail" className="col-sm-2 col-form-label">SIP Uri</label>
                    <div className="col-sm-4">
                        <input 
                            type="text" 
                            className="form-control" 
                            id="staticEmail" 
                            name="sipuri" 
                            defaultValue={localStorage.getItem("sipuri")} 
                            placeholder={"sip:0000161225028@sipclient.ttrs.in.th"} 
                            ref={register}
                        />
                    </div>
                    <label htmlFor="inputPassword" className="col-sm-2 col-form-label">Password</label>
                    <div className="col-sm-4">
                        <input 
                            type="text" 
                            className="form-control" 
                            id="inputPassword" 
                            name="password" 
                            defaultValue={localStorage.getItem("password")} 
                            placeholder={"Cmyn6jVtYd2qImUlCrnk"}
                            ref={register}/>
                    </div>
                </div>
                <div className="form-group row">
                    <label htmlFor="inputWebsocket" className="col-sm-2 col-form-label">Websocket</label>
                    <div className="col-sm-4">
                        <input 
                            type="text" 
                            className="form-control" 
                            id="inputWebsocket" 
                            name="websocket" 
                            defaultValue={localStorage.getItem("websocket")} 
                            placeholder={"wss://sipclient.ttrs.in.th:8089/ws"}
                            ref={register}
                        />
                    </div>
                    <label htmlFor="inputWebsocket" className="col-sm-2 col-form-label">Destination</label>
                    <div className="col-sm-4">
                        <input 
                            type="text" 
                            className="form-control" 
                            id="inputCestination" 
                            name="destination" 
                            defaultValue={localStorage.getItem("destination")} 
                            placeholder={"9999@sipclient.ttrs.in.th"}
                            ref={register}
                        />
                    </div>
                </div>
                {/* <div className="form-group row">
                    <label htmlFor="inputWebsocket" className="col-sm-2 col-form-label">IceServers(turn)</label>
                    <div className="col-sm-4">
                        <input type="text" className="form-control" id="inputCestination" name="iceServersUrl" placeholder="iceServersUrl" defaultValue={"turn:turn.ttrs.in.th?transport=tcp"} ref={register}/>
                    </div>
                    <div className="col-sm-3">
                        <input type="text" className="form-control" id="inputCestination" name="iceServersUsername" placeholder="Username" defaultValue={"turn01"} ref={register}/>
                    </div>
                    <div className="col-sm-3">
                        <input type="text" className="form-control" id="inputCestination" name="iceServersPassword" placeholder="Password" defaultValue={"Test1234"} ref={register}/>
                    </div>
                </div> */}
                <div className="row">
                    <div className="col">
                        <button type="submit" className="btn btn-primary btn-sm btn-block" disabled={controlEnable.registerButton} onClick={event => handleRegister()}>Register</button> 
                    </div>
                    <div className="col">
                        <button type="submit" className="btn btn-warning btn-sm btn-block" disabled={controlEnable.startCallButton} onClick={event => handleCall()}>Call</button> 
                    </div>
                    {/* <div className="col">
                        <button type="submit" className="btn btn-success btn-sm btn-block" disabled={controlEnable.startCallButton} onClick={event => handleAnswer()}>Answer</button> 
                    </div> */}
                    <div className="col">
                        <button type="submit" className="btn btn-danger btn-sm btn-block" disabled={controlEnable.disconnectButton} onClick={event => handleDisconnect()}>Disconnect</button> 
                    </div>
                    <div className="col">
                        <button type="button" className="btn btn-success btn-sm btn-block" disabled={controlEnable.toggleCameraButton} onClick={event => handleToggleCamera()}>Toggle Camera</button>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                        {sipLog}
                    </div>
                </div>
            </form>
            <div className="row">
                <div className="col-sm-6 card-video">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title"> Local</h5>
                            <video className="local-video" id="local-video" muted autoPlay playsInline/>
                        </div>
                    </div>
                </div>
                {
                    streams.map((data) => (
                        <GenerateRemoteElement 
                            stream={data} 
                            key={data.id}
                        />
                    ))
                }
            </div>
        </div>
    );
}

export default App;
